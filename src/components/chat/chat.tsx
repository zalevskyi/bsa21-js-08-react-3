import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { RootState } from '../../store/store';
import { Role } from '../../store/user/user.d';
import Authorization from '../authorization/authorization';

export function Chat() {
  const { username, role } = useSelector((state: RootState) => state.login);
  return (
    <>
      <Authorization />
      <div>{username}</div>
      {role === Role.admin && <Link to='/users'>Users list</Link>}
      <div>Chat component</div>
    </>
  );
}

export default Chat;
