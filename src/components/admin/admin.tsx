import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { AppRoute } from '../app/App.d';
import { RootState } from '../../store/store';

export function Admin() {
  const role = useSelector((state: RootState) => state.login.role);
  return (
    <>
      {role && role !== 'admin' && (
        <Redirect
          to={{
            pathname: AppRoute.chat,
          }}
        />
      )}
    </>
  );
}

export default Admin;
