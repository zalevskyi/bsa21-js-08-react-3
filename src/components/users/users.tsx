import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { RequestStatus } from '../../service/service.d';
import { RootState } from '../../store/store';
import { User } from '../../store/user/user.d';
import { deleteUser, fetchUsers } from '../../store/users/actions';
import { setForRefresh } from '../../store/users/users';
import Admin from '../admin/admin';
import { AppRoute } from '../app/App.d';
import Authorization from '../authorization/authorization';

export function Users() {
  const { username } = useSelector((state: RootState) => state.login);
  const { users, error, status, refresh } = useSelector(
    (state: RootState) => state.users,
  );
  const dispatch = useDispatch();
  console.log(status, status === RequestStatus.initial);
  if (status === RequestStatus.initial || refresh) {
    dispatch(fetchUsers());
  }
  return (
    <>
      <Authorization />
      <Admin />
      <div>{username}</div>
      <Link to={`${AppRoute.chat}`}>Chat</Link>
      {status === RequestStatus.pending && <div>Loading</div>}
      {status === RequestStatus.rejected && <div>{error}</div>}
      {status === RequestStatus.fulfilled && (
        <>
          <div>User list:</div>
          <Link
            to={AppRoute.userEdit}
            onClick={e => {
              dispatch(setForRefresh());
            }}>
            Add user
          </Link>
          {users.map(user => (
            <UserItem {...user} key={user.id} />
          ))}
        </>
      )}
    </>
  );
}

function UserItem(user: User) {
  const dispatch = useDispatch();
  return (
    <div>
      <span>{user.username} </span>
      <span>{user.role} </span>
      <Link to={`${AppRoute.userEdit}${user.id}`}>Edit </Link>
      <a
        href='delete'
        onClick={e => {
          e.preventDefault();
          dispatch(deleteUser(user.id));
        }}>
        Delete
      </a>
    </div>
  );
}

export default Users;
