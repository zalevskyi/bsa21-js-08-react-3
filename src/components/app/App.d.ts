export interface ParamsId {
  id: string;
}

export enum AppRoute {
  login = '/login',
  chat = '/',
  message = '/message/:id',
  messageEdit = '/message/',
  users = '/users',
  user = '/user/:id',
  userAdd = '/user/',
  userEdit = '/user/',
  any = '*',
}
