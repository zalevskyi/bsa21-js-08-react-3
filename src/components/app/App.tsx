import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { AppRoute } from './App.d';
import './App.css';
import Login from '../login/login';
import Users from '../users/users';
import User from '../user/user';
import Message from '../message/message';
import Chat from '../chat/chat';
import PageNotFound from '../page-not-found/page-not-found';

function App() {
  return (
    <Router>
      <Switch>
        <Route path={AppRoute.login} exact component={Login} />
        <Route path={AppRoute.chat} exact component={Chat} />
        <Route path={AppRoute.message} exact component={Message} />
        <Route path={AppRoute.users} exact component={Users} />
        <Route path={AppRoute.user} exact component={User} />
        <Route path={AppRoute.userAdd} exact component={User} />
        <Route path={AppRoute.any} exact component={PageNotFound} />
      </Switch>
    </Router>
  );
}

export default App;
