import { Link, RouteComponentProps, useHistory } from 'react-router-dom';
import { AppRoute, ParamsId } from '../app/App.d';
import Authorization from '../authorization/authorization';
import Admin from '../admin/admin';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store/store';
import {
  setPassword,
  setUsername,
  setRole,
  reset,
} from '../../store/user/user';
import { addUser } from '../../store/user/actions';
import { Role } from '../../store/user/user.d';
import { RequestStatus } from '../../service/service.d';

export function User(props: RouteComponentProps<ParamsId>) {
  const { id, username, password, role, status, error } = useSelector(
    (state: RootState) => state.user,
  );
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <>
      <Authorization />
      <Admin />
      <div>
        <div>{props.match.params.id ? 'Edit user:' : 'Create user:'}</div>
        {status === RequestStatus.pending && <div>Loading</div>}
        {status === RequestStatus.rejected && <div>{error}</div>}
        {status === RequestStatus.fulfilled ? (
          <>
            User added. Go to{' '}
            <Link
              to={AppRoute.users}
              onClick={e => {
                dispatch(reset());
              }}>
              users list
            </Link>
          </>
        ) : (
          <>
            <label>Username:</label>
            <input
              type='text'
              className='user-username'
              onBlur={({ target: { value } }) => dispatch(setUsername(value))}
            />
            <label>Password:</label>
            <input
              type='text'
              className='user-password'
              onBlur={({ target: { value } }) => dispatch(setPassword(value))}
            />
            <label>Role:</label>
            <select
              onChange={({ target: { value } }) =>
                dispatch(setRole(value as Role))
              }>
              <option value={Role.user}>user</option>
              <option value={Role.admin}>admin</option>
            </select>
            <button
              type='button'
              onClick={e => {
                e.preventDefault();
                dispatch(reset());
                history.push(AppRoute.users);
              }}>
              Cancel
            </button>
            <button
              type='button'
              onClick={e => {
                e.preventDefault();
                dispatch(
                  addUser({
                    username: username || '',
                    password: password || '',
                    role: role,
                  }),
                );
              }}>
              Add user
            </button>
          </>
        )}
      </div>
    </>
  );
}

export default User;
