import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { AppRoute } from '../app/App.d';
import { RootState } from '../../store/store';

export function Authorization() {
  const id = useSelector((state: RootState) => state.login.id);
  return (
    <>
      {!id && (
        <Redirect
          to={{
            pathname: AppRoute.login,
          }}
        />
      )}
    </>
  );
}

export default Authorization;
