import { RouteComponentProps } from 'react-router-dom';
import { ParamsId } from '../app/App.d';
import Authorization from '../authorization/authorization';

export function Message(props: RouteComponentProps<ParamsId>) {
  return (
    <>
      <Authorization />
      <div>Message component. Id: {props.match.params.id}</div>
    </>
  );
}

export default Message;
