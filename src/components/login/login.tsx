import { Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { AppRoute } from '../app/App.d';
import { RootState } from '../../store/store';
import { RequestStatus } from '../../service/service.d';
import { setUsername, setPassword } from '../../store/login/login';
import { authorize } from '../../store/login/actions';
import { Role } from '../../store/user/user.d';

export function Login() {
  const { id, username, password, role, status, error } = useSelector(
    (state: RootState) => state.login,
  );
  const dispatch = useDispatch();
  return (
    <>
      {id ? (
        <Redirect
          to={{
            pathname: role === Role.admin ? AppRoute.users : AppRoute.chat,
          }}
        />
      ) : (
        <div className='login-form'>
          {status === RequestStatus.pending && (
            <div className='login-pending'>Connecting</div>
          )}
          {error && <div className='login-error'>{error}</div>}
          <label>Username:</label>
          <input
            type='text'
            className='login-username'
            onBlur={({ target: { value } }) => dispatch(setUsername(value))}
          />
          <label>Password:</label>
          <input
            type='password'
            className='login-password'
            onBlur={({ target: { value } }) => dispatch(setPassword(value))}
          />
          <button
            type='button'
            onClick={e => {
              e.preventDefault();
              dispatch(
                authorize({
                  username: username || '',
                  password: password || '',
                }),
              );
            }}>
            Log in
          </button>
        </div>
      )}
    </>
  );
}

export default Login;
