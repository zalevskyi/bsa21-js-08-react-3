import { Link } from 'react-router-dom';

export function PageNotFound() {
  return (
    <>
      <div>
        Page not found. Go <Link to='/'>Chat</Link>
      </div>
    </>
  );
}

export default PageNotFound;
