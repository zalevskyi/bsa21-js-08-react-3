import { createSlice } from '@reduxjs/toolkit';
import { RequestStatus } from '../../service/service.d';
import { User } from '../user/user.d';
import { deleteUser, fetchUsers } from './actions';

interface UsersState {
  users: User[];
  status: RequestStatus;
  error: string | null;
  refresh: boolean;
}

const initialState: UsersState = {
  users: [],
  status: RequestStatus.initial,
  error: null,
  refresh: false,
};

export const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setForRefresh: state => {
      state.refresh = true;
    },
  },
  extraReducers: builder => {
    builder.addCase(fetchUsers.fulfilled, (state, action) => {
      state.status = RequestStatus.fulfilled;
      state.error = null;
      state.users = action.payload;
      state.refresh = false;
    });
    builder.addCase(fetchUsers.rejected, (state, { error: { message } }) => {
      state.status = RequestStatus.rejected;
      state.error = message ? message : null;
      state.refresh = false;
    });
    builder.addCase(fetchUsers.pending, state => {
      state.status = RequestStatus.pending;
      state.refresh = false;
    });
    builder.addCase(deleteUser.fulfilled, (state, { payload: { id } }) => {
      state.status = RequestStatus.fulfilled;
      state.error = null;
      state.users = state.users.filter(user => user.id !== id);
    });
    builder.addCase(deleteUser.rejected, (state, { error: { message } }) => {
      state.status = RequestStatus.rejected;
      state.error = message ? message : null;
    });
    builder.addCase(deleteUser.pending, (state, action) => {
      state.status = RequestStatus.pending;
    });
  },
});

export const { setForRefresh } = usersSlice.actions;
export default usersSlice.reducer;
