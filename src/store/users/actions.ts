import { createAsyncThunk } from '@reduxjs/toolkit';
import { apiEndpoint, apiURL } from '../../service/service.d';

export const fetchUsers = createAsyncThunk('users', async () => {
  const response = await fetch(`${apiURL}${apiEndpoint.users}`);
  if (!response.ok) {
    throw new Error(
      `Connection error: ${response.status} ${response.statusText}`,
    );
  } else {
    const responseJSON = await response.json();
    return responseJSON;
  }
});

export const deleteUser = createAsyncThunk(
  'users/delete',
  async (id: string) => {
    const response = await fetch(`${apiURL}${apiEndpoint.users}${id}`, {
      method: 'DELETE',
    });
    if (!response.ok) {
      if (response.status === 404) {
        throw new Error('Unable to delete. User not found');
      } else {
        throw new Error(
          `Connection error: ${response.status} ${response.statusText}`,
        );
      }
    } else {
      return { id };
    }
  },
);
