import { createAsyncThunk } from '@reduxjs/toolkit';
import { apiEndpoint, apiURL } from '../../service/service.d';
import { Role } from './user.d';

interface NewUser {
  username: string;
  password: string;
  role: Role;
}

export const addUser = createAsyncThunk(
  'users/delete',
  async (user: NewUser) => {
    const response = await fetch(`${apiURL}${apiEndpoint.users}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id: user.username, ...user }),
    });
    if (!response.ok) {
      if (response.status === 500) {
        throw new Error('Unable to create. Username is already in use');
      } else {
        throw new Error(
          `Connection error: ${response.status} ${response.statusText}`,
        );
      }
    } else {
      const responseJSON = await response.json();
      return responseJSON;
    }
  },
);
