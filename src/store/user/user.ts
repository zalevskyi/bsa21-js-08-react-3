import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RequestStatus } from '../../service/service.d';
import { Role } from '../user/user.d';
import { addUser } from './actions';

interface UserState {
  id: string | null;
  username: string | null;
  password: string | null;
  role: Role;
  status: RequestStatus;
  error: string | null;
}

const initialState: UserState = {
  id: null,
  username: null,
  password: null,
  role: Role.user,
  status: RequestStatus.initial,
  error: null,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUsername: (state, action: PayloadAction<string>) => {
      state.username = action.payload;
    },
    setPassword: (state, action: PayloadAction<string>) => {
      state.password = action.payload;
    },
    setRole: (state, action: PayloadAction<Role>) => {
      state.role = action.payload;
    },
    reset: state => {
      resetState(state);
    },
  },
  extraReducers: builder => {
    builder.addCase(addUser.fulfilled, (state, action) => {
      state.status = RequestStatus.fulfilled;
      state.error = null;
      state.id = action.payload.id;
    });
    builder.addCase(addUser.rejected, (state, { error: { message } }) => {
      state.status = RequestStatus.rejected;
      state.error = message ? message : null;
    });
    builder.addCase(addUser.pending, state => {
      state.status = RequestStatus.pending;
    });
  },
});

function resetState(state: UserState): void {
  state.id = null;
  state.username = null;
  state.password = null;
  state.role = Role.user;
  state.status = RequestStatus.initial;
  state.error = null;
}

export const { setUsername, setPassword, setRole, reset } = userSlice.actions;
export default userSlice.reducer;
