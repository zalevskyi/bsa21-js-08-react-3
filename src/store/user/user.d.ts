export interface User {
  id: string;
  username: string;
  password: string;
  role: Role;
}

export enum Role {
  admin = 'admin',
  user = 'user',
}
