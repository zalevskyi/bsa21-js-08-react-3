import { createAsyncThunk } from '@reduxjs/toolkit';
import { apiEndpoint, apiURL } from '../../service/service.d';

interface loginData {
  username: string;
  password: string;
}

export const authorize = createAsyncThunk(
  'authorize',
  async (data: loginData) => {
    const response = await fetch(
      `${apiURL}${apiEndpoint.users}?username=${data.username}&password=${data.password}`,
    );
    if (!response.ok) {
      throw Error(
        `Connection error: ${response.status} ${response.statusText}`,
      );
    } else {
      const responseJSON = await response.json();
      if (responseJSON.length === 1) {
        return responseJSON[0];
      } else {
        throw Error(`No such user or password incorrect`);
      }
    }
  },
);
