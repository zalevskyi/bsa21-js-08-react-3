import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RequestStatus } from '../../service/service.d';
import { Role } from '../user/user.d';
import { authorize } from './actions';

interface LoginState {
  id: string | null;
  username: string | null;
  password: string | null;
  role: Role;
  status: RequestStatus;
  error: string | null;
}

const initialState: LoginState = {
  id: null,
  username: null,
  password: null,
  role: Role.user,
  status: RequestStatus.initial,
  error: null,
};

export const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    setUsername: (state, action: PayloadAction<string>) => {
      if (state.status !== RequestStatus.initial) {
        resetState(state);
      }
      state.username = action.payload;
    },
    setPassword: (state, action: PayloadAction<string>) => {
      if (state.status !== RequestStatus.initial) {
        resetState(state);
      }
      state.password = action.payload;
    },
  },
  extraReducers: builder => {
    builder.addCase(authorize.fulfilled, (state, action) => {
      state.status = RequestStatus.fulfilled;
      state.error = null;
      state.id = action.payload.id;
      state.role = action.payload.role;
    });
    builder.addCase(authorize.rejected, (state, { error: { message } }) => {
      state.status = RequestStatus.rejected;
      state.error = message ? message : null;
    });
    builder.addCase(authorize.pending, state => {
      state.status = RequestStatus.pending;
    });
  },
});

function resetState(state: LoginState): void {
  state.id = null;
  state.role = Role.user;
  state.error = null;
  state.status = RequestStatus.initial;
}

export const { setUsername, setPassword } = loginSlice.actions;
export default loginSlice.reducer;
