import { configureStore } from '@reduxjs/toolkit';
import loginReducer from './login/login';
import usersReducer from './users/users';
import userReducer from './user/user';

export const store = configureStore({
  reducer: { login: loginReducer, users: usersReducer, user: userReducer },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
