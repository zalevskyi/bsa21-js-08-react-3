export enum RequestStatus {
  initial,
  pending,
  fulfilled,
  rejected,
}

export const apiURL = 'http://localhost:3001';

export enum apiEndpoint {
  users = '/users/',
  messages = '/messages/',
}
